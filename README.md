﻿# PROCRASTINATHOR

# DESCRIPTION
Game made in 48h to the GameJam UFPE Cin organized by the Federal University of Pernambuco, in August 17-19, 2018. 
My main role in this project was as programmer, using Unreal 4 as engine. 
The theme of the jam was "Time".
The game was made to inspire the old style arcade sensation inside the player, but with touches of modernity.
It was a challange to me to be creating a whole 2D game in Unreal.
The player needs to pay attention to the bullet hell as he shoots his spaceship only if he is able to press the shoot button 
synced with the music beat.

# TEAM
Programmers: Marcelino Borges, Gustavo Mesel
3D Artists: Caio Filizola
2D Artist: Carlos Eduardo, Matheus Alencar
SFX & Music: Nara Araújo
Manager: Lucas Pires
Game Designer: the whole team

# ArtStation
https://www.artstation.com/artwork/mradZ

#YOUTUBE
https://youtu.be/qfCUywQSJf8

